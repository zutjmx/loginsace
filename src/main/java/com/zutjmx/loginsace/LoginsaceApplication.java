package com.zutjmx.loginsace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginsaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginsaceApplication.class, args);
	}

}
