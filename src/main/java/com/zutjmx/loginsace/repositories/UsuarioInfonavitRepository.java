package com.zutjmx.loginsace.repositories;

import com.zutjmx.loginsace.entities.UsuarioInfonavit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioInfonavitRepository extends JpaRepository<UsuarioInfonavit, String> {
}
