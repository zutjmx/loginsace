package com.zutjmx.loginsace.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RespuestaLogin {
    private String conexion;
    private String usuario;
    private String status;
    private String mail;
    private String apellidos;
    private String nombre;
}
