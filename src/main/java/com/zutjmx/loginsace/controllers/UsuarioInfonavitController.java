package com.zutjmx.loginsace.controllers;

import com.github.javafaker.Faker;
import com.zutjmx.loginsace.entities.UsuarioInfonavit;
import com.zutjmx.loginsace.models.RespuestaLogin;
import com.zutjmx.loginsace.models.UsuarioModel;
import com.zutjmx.loginsace.repositories.UsuarioInfonavitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/api/autentica")
public class UsuarioInfonavitController {
    @Autowired
    private UsuarioInfonavitRepository usuarioInfonavitRepository;

    @GetMapping("/usuarios")
    public ResponseEntity<List<UsuarioInfonavit>> getAllEmployees() {
        return new ResponseEntity<>(usuarioInfonavitRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ResponseEntity<RespuestaLogin> login(@ModelAttribute("usuario")UsuarioModel usuarioModel) {
        RespuestaLogin respuestaLogin = new RespuestaLogin();
        Faker faker = new Faker();
        Optional<UsuarioInfonavit> optionalUsuarioInfonavit = usuarioInfonavitRepository.findById(usuarioModel.getUsuario());
        if (optionalUsuarioInfonavit.isPresent()) {
            UsuarioInfonavit usuarioInfonavit = optionalUsuarioInfonavit.get();
            respuestaLogin.setConexion("exitosa");
            respuestaLogin.setUsuario(usuarioInfonavit.getEmployeeId());
            respuestaLogin.setStatus("verificado");
            respuestaLogin.setMail(faker.internet().emailAddress());
            respuestaLogin.setApellidos(usuarioInfonavit.getPaterno().concat(" ").concat(usuarioInfonavit.getMaterno()));
            respuestaLogin.setNombre(usuarioInfonavit.getPaterno()
                    .concat(" ")
                    .concat(usuarioInfonavit.getMaterno())
                    .concat(" ")
                    .concat(usuarioInfonavit.getNombre()));
            return new ResponseEntity<>(respuestaLogin,HttpStatus.OK);
        } else {
            respuestaLogin.setConexion("exitosa");
            respuestaLogin.setUsuario(usuarioModel.getUsuario());
            respuestaLogin.setStatus("no verificado");
            respuestaLogin.setMail("");
            respuestaLogin.setApellidos("");
            respuestaLogin.setNombre("");
            return new ResponseEntity<>(respuestaLogin,HttpStatus.OK);
        }
    }
}
